
class Bunch(object):
    """Class to allow 'x.foo' access to dictionary 'adict'"""
    def __init__(self, adict):
        self.__dict__.update(adict)
