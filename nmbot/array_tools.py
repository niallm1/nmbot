import numpy as np
import fitsio

def remove_field_name(a, rm_names):
    names = list(a.dtype.names)
    for rm_name in rm_names:
        if rm_name in names:
            names.remove(rm_name)
    b = a[names]
    return b

def add_field(a, descr, arrays):
    """Return a new array that is like "a", but has additional fields.

    Arguments:
      a     -- a structured numpy array
      descr -- a numpy type description of the new fields

    The contents of "a" are copied over to the appropriate fields in
    the new array, whereas the new fields are uninitialized.  The
    arguments are not modified.

    >>> sa = numpy.array([(1, 'Foo'), (2, 'Bar')], \
                         dtype=[('id', int), ('name', 'S3')])
    >>> sa.dtype.descr == numpy.dtype([('id', int), ('name', 'S3')])
    True
    >>> sb = add_field(sa, [('score', float)])
    >>> sb.dtype.descr == numpy.dtype([('id', int), ('name', 'S3'), \
                                       ('score', float)])
    True
    >>> numpy.all(sa['id'] == sb['id'])
    True
    >>> numpy.all(sa['name'] == sb['name'])
    True
    """
    if a.dtype.fields is None:
        raise(ValueError, "`A' must be a structured numpy array")
    b = np.empty(a.shape, dtype=a.dtype.descr + descr)
    for name in a.dtype.names:
        b[name] = a[name]
    for d,c in zip(descr,arrays):
        b[d[0]] = c
    return b

def vstack2(array1, array2):
    if array1.dtype.fields is None:
        raise(ValueError, "`array1' must be a structured numpy array")
    if array2.dtype.fields is None:
        raise(ValueError, "`array2' must be a structured numpy array")
    if array1.shape!=array2.shape:
        raise(ValueError, "input arrays must have same shape")
    out = np.empty(array1.shape, dtype=array1.dtype.descr + array2.dtype.descr)
    for arr in [array1,array2]:
        for name in arr.dtype.names:
            out[name] = arr[name]
    return out

def hstack2(arrays):
    return arrays[0].__array_wrap__(np.hstack(arrays))

def to_array(recarray,colnames=None):
    """Recarray to numpy array"""
    if colnames is None:
        colnames=recarray.dtype.names
    array_out=np.zeros((len(colnames),len(recarray)))
    for i,n in enumerate(colnames):
        array_out[i]=recarray[n]
    return array_out

def fits_read_rand_frac(filename,rand_frac,ext=1,columns=None):
    assert rand_frac<=1.
    assert rand_frac>=0.
    f=fitsio.FITS(filename)[ext]
    nr=int(f.get_nrows())
    rows=np.random.choice(np.arange(nr),size=int(rand_frac*nr),replace=False)
    return f.read(rows=rows,columns=columns),rows

def apply_cut_dict(in_array, cut_dict, inclusive=True, verbose=True, func_dict=None):
    use = np.ones(len(in_array),dtype=bool)
    for key,val in cut_dict.iteritems():
        if len(val)==1:
            mask = in_array[key]==val[0]
            cut_string = "{0} == {1}".format(key, val[0])
        elif len(val)==2:
            if inclusive:
                mask = (in_array[key]>=val[0])*(in_array[key]<=val[1])
                cut_string = "{0} <= {1} <= {2}".format(val[0], key, val[1])
            else:
                mask = (in_array[key]>val[0])*(in_array[key]<val[1])
                cut_string = "{0} < {1} < {2}".format(val[0], key, val[1])
        else:
            raise(ValueError, "cut value should be of length 1 or 2")
        use[~mask] = False
        if verbose:
            print('applying {0} leaves fraction {1}'.format(cut_string, float(mask.sum())/len(mask)))
    if verbose:
        print('all cuts leave fraction {0}'.format(float(use.sum())/len(use)))
    return in_array[use], use
