import fitsio
import sys


def main():
    f=fitsio.FITS(sys.argv[1])
    print(f)

    for ext in f:
        try:
            header,data= ext.read_header(), ext.read()
        except TypeError:
            header,data=None,None
        print(ext)
        print('###############')
        print('HEADER:')
        print(header)
        print('DATA:')
        if data is None: 
            print(data)
            continue
        print(len(data))
        if len(data)>36:
            print(data[:5])
            print("......")
            print(data[-5:])
        else:
            print(data)

if __name__=="__main__":
    main()
