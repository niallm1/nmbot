import numpy as np

def binned_correlation(x, y, bins=15, percentiles=None):
    if percentiles:
        bins=np.linspace(np.percentile(x,percentiles),np.percentile(x,100.-percentiles),bins)
    n_in_bin, bin_lims = np.histogram(x, bins=bins)
    ysum, _ = np.histogram(x, bins=bins, weights=y)
    ymean = ysum.astype(float)/n_in_bin
    y2sum, _ = np.histogram(x, bins=bins, weights=y*y)
    print('n_in_bin',n_in_bin)
    return 0.5*(bin_lims[:-1]+bin_lims[1:]), bin_lims, ymean,  np.sqrt(y2sum.astype(float)/n_in_bin - ymean**2)/np.sqrt(n_in_bin)

def binned_correlation2(x,y,w=None,bins=None):
    """We want to correlate x and y, so calculated mean and 
    error on mean of y, in bins of x"""
    if w is None:
        w=np.ones_like(x)
    n_in_bin,bin_lims=np.histogram(x,bins=bins)
    Sum_w,_=np.histogram(x,bins=bin_lims,weights=w)
    Mean_w=Sum_w/n_in_bin
    #use digitize and apply a couple of corrections to get bin index for each element
    bin_inds=np.digitize(x,bins=bin_lims)-1
    bin_inds[bin_inds==len(Sum_w)]=len(Sum_w)-1
    #get weighted mean of each bin
    Ysum_w,_=np.histogram(x,bins=bin_lims,weights=y*w)
    Ymean_w = Ysum_w/n_in_bin
    ymean_w = Ymean_w[bin_inds]
    #weighted sd
    if w is None:
        Y2sum = np.histogram(x, bins=bin_lims, weights=(y-y_mean_w)**2)
        Ymean_err = np.sqrt(Y2sum/(n_in_bin-1))/np.sqrt(n_in_bin)
        return 0.5*(bin_lims[:-1]+bin_lims[1:]),Ymean_w,Ymean_err,bin_lims,n_in_bin
    A,_ = np.histogram(x,bins=bin_lims, weights=(w*y-Mean_w[bin_inds]*Ymean_w[bin_inds])**2)
    B = -2 * Ymean_w * np.histogram(x, bins=bin_lims, weights = (w - Mean_w[bin_inds])*(w*y - Mean_w[bin_inds]*Ymean_w[bin_inds]))[0]
    C = Ymean_w**2 * np.histogram(x, bins=bin_lims, weights= (w - Mean_w[bin_inds])**2)[0]
    D = np.histogram(x, bins=bin_lims, weights=w)[0]
    Ymean_err = n_in_bin * (A + B + C) / D**2 / (n_in_bin-1)
    return 0.5*(bin_lims[:-1]+bin_lims[1:]),Ymean_w,Ymean_err,bin_lims,n_in_bin
    
