import fitsio
from os.path import join as pj

def fits_read_rand_frac(filename,rand_frac,ext=1,columns=None):
    assert rand_frac<=1.
    assert rand_frac>=0.
    f=fitsio.FITS(filename)[ext]
    nr=int(f.get_nrows())
    rows=np.random.choice(np.arange(nr),size=int(rand_frac*nr),replace=False)
    return f.read(rows=np.copy(rows),columns=columns),rows
    
