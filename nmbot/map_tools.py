#tools for healpix maps
import healpix_util as hu
import numpy as np
import fitsio
MASKED_VAL=-999

def read_partial_map(filename,ext=1,masked_val=MASKED_VAL,pix_col='PIXEL',val_col='SIGNAL',scheme="ring"):
    f=fitsio.FITS(filename)[ext]
    nside=int(f.read_header()['NSIDE'])
    hpix=hu.HealPix(scheme,nside)
    m=masked_val*np.ones(hpix.npix)
    m_data=f.read()
    m[m_data[pix_col]]=m_data[val_col]
    return hu.Map("ring", m)


def humap2partial(hmap, masked_val):
    pixinds = np.arange(hmap.data.size)
    use = (hmap.data!=masked_val)
    

def hu_to_partial(hmaps, mask, pix_col='PIXEL',val_cols=['SIGNAL']):
    """mask should be zeros or ones"""
    assert mask.shape==hmap.data.shape
    assert np.all(np.unique(mask) in [0,1])
    npix=hmap.data.size
    pixinds = np.arange(npix)
    use_inds = (mask==1)
    partial_map, partial_inds = pixinds[use_inds], hmap.data[use_inds]
    #out_array = np.zeros((, 
    #dataview=hmap.data.view(view_dtype)
    #with fitsio.FITS(filename,'rw',**kw) as fits:
